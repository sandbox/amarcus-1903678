<?php

/**
 * Select a tab the next time quicktabs loads.
 * @param string $qt_name The machine name of the quicktabs instance.
 * @param string $tab_name The index or machine name of the tab to select.
 */
function quicktabs_select_tab($qt_name, $tab_name) {
  $selected = &drupal_static('quicktabs_select_tab');
  
  $selected[$qt_name] = $tab_name;
  $_SESSION['quicktabs_select'] = $selected;
  
  return $selected;
}

/**
 * Gets the tab that was most recently selected.
 * 
 * @param string $qt_name The machine name of the quicktabs instance.
 * @param boolean $clear If set to false, the selected tab will not be removed from the session.
 * @return string The index or machine name of the tab to select.
 */
function quicktabs_get_selected_tab($qt_name = NULL, $clear = TRUE) {
  $selected = &drupal_static('quicktabs_select_tab');
  if (isset($_SESSION['quicktabs_select'])) {
    if (!isset($selected)) {
      $selected = $_SESSION['quicktabs_select'];
    }
    if ($clear) {
      unset($_SESSION['quicktabs_select']);
    }
  }
  if (isset($qt_name)) {
    if (isset($selected[$qt_name])) {
      return $selected[$qt_name];
    }
    return NULL;
  }
  return $selected;
}

/**
 * Implements hook_init().
 * 
 * Gets the selected tab from the session and then removes it.
 * This means that a tab selection will only survive for one page
 * request, even if quicktabs is never loaded on that page.
 */
function quicktabs_select_init() {
  quicktabs_get_selected_tab();
}

/**
 * Implements hook_quicktabs_alter().
 * 
 * @param object $quicktabs The quicktabs instance to render
 */
function quicktabs_select_quicktabs_alter($quicktabs) {
  $qt_name = $quicktabs->machine_name;
  
  $default = quicktabs_get_selected_tab($qt_name);
  if (!empty($default)) {
    $index = quicktabs_select_get_tab_index($qt_name, $default);
    $quicktabs->default_tab = $index;
  }
}

function quicktabs_select_get_tab_index($qt_name, $selected) {
  if (is_numeric($selected)) {
    return $selected;
  }
  $qt = quicktabs_load($qt_name);
  if (isset($qt)) {
    foreach ($qt->tabs as $i => $tab) {
      if ($tab['bid'] == $selected) {
        return $i;
      }
    }
  }
  return 0;
}

function quicktabs_select_url_alter($qt_name, $selected, &$url, &$options = array()) {
  if (!isset($url)) {
    return NULL;
  }
  if (empty($options) && is_array($url)) {
    if (!isset($url[1])) {
      $url[1] = array();
    }
    $options = &$url[1];
  }
  
  // Lookup the tab index and add it to the URL
  $index = quicktabs_select_get_tab_index($qt_name, $selected);  
  $options['query']['qt-' . $qt_name] = $index;
  
  return $url;
}